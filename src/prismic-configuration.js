export default {
  apiEndpoint: 'https://prelaunch.cdn.prismic.io/api/v2',

  linkResolver(doc) {
    if (doc.type === 'landing_page') return `/landing/${doc.uid}`;
    
    return '/';
  },
};
