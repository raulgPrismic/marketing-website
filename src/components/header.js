import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import Logo from '../static/img/logo.svg'

class Header extends Component {
  render() {
    return (
      <header className='background-color-492 width-pc-553'>
        <a id='desktop-logo' href='/'>
          <img id='logo' className='margin-left-166' src={Logo}/>
        </a>
        <a id='mobile-logo' href='/'>
          <img id='logo' className='margin-left-166' src={Logo}/>
        </a>
        <Link to={{pathname: '/apply'}} id="desktop-menu-option" className="float-right color-738 margin-left-166 margin-right-166">Become a barber</Link>
      </header>
    )
  }
}

export default Header;
