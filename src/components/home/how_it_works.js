import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import '../../static/css/how_it_works.css';

class How_it_works extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'how_it_works')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const document = this.state.doc.data;
            return (
                <div>
                    <div className="background-color-738 horizontal-center">
                        <div id="how-it-works-1-center" className="max-width-px-257 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                            <div id="how-it-works-1-center-1" className="vertical-center horizontal-center">
                                <div>
                                    <h3 className="color-492 margin-bottom-lh-361">{RichText.asText(document.title_1)}</h3>
                                    <p className="color-366">{RichText.asText(document.description_1)}</p>
                                </div>
                            </div>
                            <div id="how-it-works-1-center-2" className="vertical-center horizontal-center">
                                <img className="height-px-155 shadow border-radius-149" src={document.image_1.url}></img>
                            </div>
                        </div>
                    </div>
                    <div className="background-color-191 horizontal-center">
                        <div id="how-it-works-2-center" className="max-width-px-257 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                            <div id="how-it-works-2-center-1" className="vertical-center horizontal-center">
                                <img className="height-px-155 shadow border-radius-149" src={document.image_2.url}></img>
                            </div>
                            <div id="how-it-works-2-center-2" className="vertical-center horizontal-center">
                                <div>
                                    <h3 className="color-492 margin-bottom-lh-361">{RichText.asText(document.title_2)}</h3>
                                    <p className="color-366">{RichText.asText(document.description_2)}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="background-color-738 horizontal-center">
                        <div id="how-it-works-3-center" className="max-width-px-257 padding-left-166 padding-right-166 padding-top-421 padding-bottom-421">
                            <div id="how-it-works-3-center-1" className="vertical-center horizontal-center">
                                <div>
                                    <h3 className="color-492 margin-bottom-lh-361">{RichText.asText(document.title_3)}</h3>
                                    <p className="color-366">{RichText.asText(document.description_3)}</p>
                                </div>
                            </div>
                            <div id="how-it-works-3-center-2" className="vertical-center horizontal-center">
                                <img className="width-pc-765" src={document.image_3.url}></img>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default How_it_works;
