import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Form from '../home/form';

class Viewport extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.id', 'XJ35EhIAAEnsD7Vl')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const document = this.state.doc.data;
            const backgroundImage = { backgroundImage: "url(" + document.image.url + ")" };
            return (
                <div id='home_viewport' style={backgroundImage} className='vertical-center horizontal-center padding-left-166 padding-right-166'>
                    <div className='max-width-px-655 padding-top-361 padding-bottom-361'>
                        <h1 className='color-738 margin-bottom-lh-166'>{RichText.asText(document.h1)}</h1>
                        <p className='color-738 margin-bottom-lh-166'>{RichText.asText(document.h2)}</p>
                        <Form/>
                    </div>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Viewport;
