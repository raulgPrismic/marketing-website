import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import Instagram from '../static/img/instagram.svg'

class Footer extends Component {
  render() {
    return (
        <footer className="background-color-492">
          <div id="footer-div-1">
              <h6 className="color-738 margin-top-lh-361 margin-bottom-lh-166">Cutter's Club</h6>
          </div>
          <div id="footer-div-2">
              <h6 className="color-738 margin-top-lh-361 margin-bottom-lh-166">Community</h6>
          </div>
          <div id="footer-div-3">
              <h6 className="color-738 margin-top-lh-361 margin-bottom-lh-166">Barbers</h6>
          </div>
          <div id="footer-div-4"></div>
          <div id="footer-div-5"></div>
          <div id="footer-div-6">
              <p>
                  <Link to={{pathname: '/apply'}} className="color-738">Become a barber</Link>
              </p>
          </div>
          <div id="footer-div-7" className="margin-top-361 margin-bottom-361">
              <a href="https://www.facebook.com/joincuttersclub" target="_blank">
                  <img id="social-icon" className="height-px-166 margin-left-166 margin-right-166" src={ Instagram }></img>
              </a>
              <a href="https://www.instagram.com/joincuttersclub/" target="_blank">
                  <img id="social-icon" className="height-px-166 margin-left-166 margin-right-166" src={ Instagram }></img>
              </a>
          </div>
        </footer>
    )
  }
}

export default Footer;
