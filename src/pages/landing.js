import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Header from '../components/header';
import Footer from '../components/footer';
import Form from '../components/home/form';
import PrismicConfig from '../prismic-configuration';
// This way we can call both the apiEndpoint and linkResolver as needed without redefining it in every page
import '../static/css/home.css';

class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            doc: null,
        }
    }

    componentWillMount() {
        Prismic.api(PrismicConfig.apiEndpoint).then(api => {
            // We fetch by the UID of the document with type 'landing_page' so this page is a proper template
            api.getByUID('landing_page', this.props.match.params.uid).then(response => {
                if (response) {
                    this.setState({ doc: response });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const backgroundImage = { backgroundImage: "url(" + this.state.doc.data.image.url + ")" };
            return (
                <div>
                    <Header/>
                    <div id='home_viewport' style={backgroundImage} className='vertical-center horizontal-center padding-left-166 padding-right-166'>
                        <div className='max-width-px-655 padding-top-361 padding-bottom-361'>
                            <h1 className='color-738 margin-bottom-lh-166'>{RichText.asText(this.state.doc.data.h1)}</h1>
                            <p className='color-738 margin-bottom-lh-166'>{RichText.asText(this.state.doc.data.h2)}</p>
                            {/* We can template rich text with RichText.render as well */}
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        }
        return <h1>Loading...</h1>;
    }
}

export default Landing;
