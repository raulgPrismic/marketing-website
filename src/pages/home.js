import React, {Component} from 'react';
import Header from '../components/header';
import Viewport from '../components/home/viewport';
import Footer from '../components/footer';
import Form from '../components/home/form';
import How_it_works from '../components/home/how_it_works';
import '../static/css/home.css';

class Home extends Component {

    render() {
        return (
            <div>
                <Header/>
                <Viewport/>
                <How_it_works/>
                <Footer/>
            </div>
        )
    }
}

export default Home;
