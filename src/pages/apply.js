import React, {Component} from 'react';
import Prismic from 'prismic-javascript';
import {Link, RichText, Date} from 'prismic-reactjs';
import Header from '../components/header';
import Footer from '../components/footer';
import Form from '../components/apply/form';
import '../static/css/home.css';

class Home extends Component {

    state = {
        doc: null,
    }

    componentWillMount() {
        const apiEndpoint = 'https://prelaunch.cdn.prismic.io/api/v2';

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'viewport')).then(response => {
                if (response) {
                    this.setState({ doc: response.results[0] });
                }
            });
        });
    }

    render() {
        if (this.state.doc) {
            const backgroundImage = { backgroundImage: "url(" + this.state.doc.data.image.url + ")" };
            return (
                <div>
                    <Header/>
                    <div id='home_viewport' style={backgroundImage} className='vertical-center horizontal-center padding-left-166 padding-right-166'>
                        <div className='max-width-px-655 padding-top-361 padding-bottom-361'>
                            <h1 className='color-738 margin-bottom-lh-166'>{RichText.asText(this.state.doc.data.h1)}</h1>
                            <p className='color-738 margin-bottom-lh-166'>{RichText.asText(this.state.doc.data.h2)}</p>
                            <Form/>
                        </div>
                    </div>
                    <Footer/>
                </div>
            );
        }
        return <h1></h1>;
    }
}

export default Home;
